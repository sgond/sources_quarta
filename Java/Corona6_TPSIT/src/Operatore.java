import java.util.Random;

public class Operatore extends Thread{
    private ContoCorrente conto;
    private int nOperazioni;
    private int somma;
    private String nomeOp;

    Random random = new Random();

    public Operatore(ContoCorrente conto, int nOperazioni, int somma, String nomeOp) {
        this.conto = conto;
        this.nOperazioni = nOperazioni;
        this.somma = somma;
        this.nomeOp = nomeOp;
    }

    @Override
    public void run() {
        for (int i = 0; i < nOperazioni; i++)
                try {
                    int tipoOp = random.nextInt(2);
                    if(tipoOp == 0)
                        conto.prelievo(somma);
                    else
                        conto.deposito(somma);

                    conto.addOperazione(somma, nomeOp, tipoOp);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
    }

    public String getNomeOp() {
        return nomeOp;
    }
}
