import java.util.ArrayList;

public class ContoCorrente {

    private String intestatario;
    private int saldo;
    private ArrayList<DatoOperazione> datiOperazioni = new ArrayList<>();

    ContoCorrente(String intestatario, int saldo){
        this.intestatario = intestatario;
        this.saldo = saldo;
    }

    public synchronized void deposito(int importo) throws InterruptedException {
        //System.out.println("Deposito in corso...");
        Thread.sleep(100);
        saldo += importo;
        //System.out.println("Deposito andato a buon fine!");
    }

    public synchronized void prelievo(int prelievo) throws InterruptedException {
        //System.out.println("Prelievo in corso...");
        Thread.sleep(100);
        saldo -= prelievo;
        //System.out.println("Prelievo andato a buon fine!");
    }

    protected void addOperazione(int sommaOp, String nomeOp, int tipoOp){
        datiOperazioni.add(new DatoOperazione(sommaOp, nomeOp, tipoOp));
    }

    public String getIntestatario() {
        return intestatario;
    }

    public int getSaldo() {
        return saldo;
    }

    public ArrayList<DatoOperazione> getDatiOperazioniAL() {
        return datiOperazioni;
    }

    public String getDatiOperazioni() {
        String out = "\r\nOperazioni del conto corrente di " + intestatario + ":";
        for (DatoOperazione d:
             datiOperazioni) {
                out += d.toString();
        }

        return out;
    }

    @Override
    public String toString() {
        return "\r\nConto bancario di "+ intestatario + "\r\n"
                +"Saldo di " + saldo + " euro\r\n";
    }

    private class DatoOperazione {
        private int sommaOp;
        private String nomeOp;
        private int tipoOp;

        public DatoOperazione(int sommaOp, String nomeOp, int tipoOp) {
            this.sommaOp = sommaOp;
            this.nomeOp = nomeOp;
            if(tipoOp <= 0)
                this.tipoOp = 0;
            else
                this.tipoOp = 1;
        }

        public String getNomeOp() {
            return nomeOp;
        }

        public int getSommaOp() {
            return sommaOp;
        }

        @Override
        public String toString() {
            if(tipoOp == 0)
                return "\r\n***Prelievo di "+ nomeOp + ", di somma: " + sommaOp + "***";
            else
                return "\r\n***Deposito di "+nomeOp + ", di somma: " + sommaOp + "***";
        }
    }
}
