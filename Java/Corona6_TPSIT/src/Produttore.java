public class Produttore extends Thread{
    ContoCorrente conto;
    int nDepositi;
    int importo;

    public Produttore(ContoCorrente conto, int nDepositi, int importo) {
        this.conto = conto;
        this.nDepositi = nDepositi;
        this.importo = importo;
    }

    @Override
    public void run() {
        for (int i = 0; i < nDepositi; i++)
            try {
                conto.deposito(importo);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }
}
