public class Main {
    public static void main(String[] args) throws InterruptedException {
        int nOperatori = 10;

        ContoCorrente conto = new ContoCorrente("Marco", 50);
        System.out.println(conto);

        Operatore[] operatori = new Operatore[nOperatori];
        for (int i = 0; i < nOperatori; i++) {
            operatori[i] = new Operatore(conto, 10, 10, "Operatore" + i);
            operatori[i].start();
        }

        for (Operatore o :
                operatori) {
            o.join();
        }

        System.out.println(conto.getDatiOperazioni());
        System.out.println(conto);
    }
}
