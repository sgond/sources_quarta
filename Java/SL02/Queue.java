/**
 * Queue
 */
public class Queue<T> {

    private Node head;
    private int size;

    Queue(){}

    private class Node{
        Node next;
        Node prev;
        T s;

        Node(T s){
                this.s=s;
        }

        Node(Node next, T s){
                this.next=next;
                this.s=s;
        }

        @Override
        public String toString() {
            return "Node [next=" + next + ", prev=" + prev + ", s=" + s + "]";
        }

        
    }

    void enqueue(T s){
        size++;
        if(head==null){
            head=new Node(s);
        }
       
        else{  
            Node n=new Node(head, s);
            head.prev=n;
            head=n;
        }
    }

    Node dequeue(){
        Node n=head;

        while(n.next!=null){
                n=n.next;
        }
        (n.prev).next=null;
        size--;

        return n;
    }

    @Override
    public String toString() {
        Node n=head;
        String out="";
        while(n!=null){
                out=out+" "+n.s;
                n=n.next;
        }
        return out;
    }

    public Node getHead() {
        return head;
    }

    public int getSize() {
        return size;
    }
}