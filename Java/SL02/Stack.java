/**
 * Stack
 */
public class Stack <T> {
    private T[] array = (T[])new Object[10];
    private int size=0;

    public Stack() {}
    
    void push(T s){
        shiftR();
        array[0]=s;
    }

    T pop(){
        T out=array[0];
        shiftL();
        return out;
    }

    private void resize(){

            T[] a=(T[])new Object[size+10];

            for (int i = 0; i < array.length; i++) 
                    a[i]=array[i];
            
            array=a;
    }

    private void shiftR(){
        if(size<array.length)    
                for (int i = size-1; i > 0; i--){
                        array[i+1]=array[i]; 
                }
        else{
                resize();
                shiftR();
        }    
    }

    private void shiftL(){
            for(int i=0; i<size; i++){
                array[i]=array[i++];
            }
    }

}