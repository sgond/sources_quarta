import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        int[] array = arrayGenerator(10);
        System.out.println(Arrays.toString(array));

        ThreadSort ts1 = new ThreadSort(array);

        System.out.println(Arrays.toString(ts1.sort()));
    }

    public static int[] arrayGenerator(int size){
        Random r = new Random();

        int[] out = new int[size];
        for (int i = 0; i < size; i++) {
            out[i] = r.nextInt(size);
        }

        return out;
    }
}