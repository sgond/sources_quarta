
import java.awt.*; 
import javax.swing.*;               // CREA UNA PROGRESS BAR 
import java.awt.event.*; 

public class Progress{ 
    static Timer t;
    public static void main(String arg[]) throws Exception
    {         
        JFrame f = new JFrame("ProgressBar demo");    
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setBounds(200,100,400,600);           // posizione e dimensione del frame              
        f.setLayout(null);                      // impaginazione        
                                                // crea una progressbar
        JProgressBar bar = new JProgressBar(JProgressBar.HORIZONTAL,0,10000); 
        bar.setValue(0);                        // valore da cui iniziare   
		// bar.setMaximum(100);                    // valore superiore della barra 
	    // bar.setMinimum(10);                    	// valore inferiore della barra
        bar.setStringPainted(true);             // mostra la percentuale sulla barra

       
        JTextArea ta= new JTextArea ("",10,50); // crea una area di testo con 10 righe e 50 colonne
        ta.setEditable(false);                  // la TextArea non � modificabile dall'utente
        String Testo= "\nSesso:\n 1 - Maschio\n 2 - Femminaòòòòòòòòòòòòòòòòòòòòòòòòòòòòòòòòòòòòòòòò";
        ta.setText(Testo);                      // modifica il contenuto dell'area di testo

        JButton bottone = new JButton("Invia");
        
        bottone.addActionListener( new ActionListener()     // Ascoltatore evento bottone premuto
                                     {
                                         public void actionPerformed(ActionEvent e)
                                         {
                                             String Comando=e.getActionCommand();
                                             if (Comando.equals("Invia"))
                                             {
                                                 JOptionPane.showMessageDialog(null, "Col mouse hai selezionato il bottone");
                                             }
                                          }
                                    });
             
        JLabel l= new JLabel("Scegli una Risposta (Numero):");

        JTextField ris= new JTextField(3);
        
        ta.setBounds(20,50,320,200);            //  (posx, posy, larghezza, altezza)
        f.add(ta);                              //  aggiunge i vari componenti al Frame
        
        bar.setBounds(100,300,130,30);
        f.add(bar);
        
        l.setBounds(100,350,200,30);                                    
        f.add(l);
        
        ris.setBounds(100,400,130,30);
        f.add(ris);
                                    
        bottone.setBounds(100,450,130,30);                            
        f.add(bottone);
 
        f.setVisible(true);

        t=new Timer(2,new ActionListener()          // ascoltatore per il timer 
                    {                               // che viene eseguito ogni 2 milli-secondi      
                       public void actionPerformed(ActionEvent ae) 
                       {
                        try {Thread.sleep(1000);}
                        catch(Exception e){}
                        if (bar.getValue() > bar.getMaximum())  t = null;                           
                        else bar.setValue(bar.getValue() + 1000);  //incrementa la barra  
                       }
                    }
                );
 
        t.start();			

    } 
    

} 