package Java.SL06;

/**
 * BST
 */
public class BST<K extends Comparable<K>, V>{

    Node root;

    private class Node{
        Node left, right;
        K key;
        V value;

        Node(K key, V value){
                this.key = key;
                this.value = value;
        }

        Node(Node left, Node right, K key, V value){
                this.left=left;
                this.right=right;
                this.key=key;
                this.value=value;
        }

        /**
         * @param left the left to set
         */
        public void setLeft(Node left) {
            this.left = left;
        }

        /**
         * @param right the right to set
         */
        public void setRight(Node right) {
            this.right = right;
        }

        @Override
        public String toString() {
            return "Node [left="+"(key:" + left.key +", value:" + left.value+")" + "right="+"(key:" + right.key +", value:" + right.value+")"+"]";
        }

        public Node getLeft() {
            return left;
        }

        public Node getRight() {
            return right;
        }

        public K getKey() {
            return key;
        }

        public void setKey(K key) {
            this.key = key;
        }

        public V getValue() {
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }

        @Override
        public int compareTo(K key){

        }


    }

    public BST(BST<K, V>.Node root) {
        this.root = root;
    }

    public BST() {}

    void checkRoot(K key, V value){
        if (root == null) 
            root= new Node(key, value);
    }

    void put(K key, V value){
        checkRoot(key, value);

        Node n=root;
        while (n!=null) {
            if(key<)
        }
    }





    
}