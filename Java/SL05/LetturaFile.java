import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 * LetturaFile
 */

public class LetturaFile {
    
    static void readWords(String fileName, HashMap words) throws IOException{
        FileReader rdr = new FileReader(fileName);

        int x = rdr.read();

        while(x!=-1){
                String s;
                char c;

                while(c!=' '){
                        c = (char) x;
                        s = s+c;
                        x = rdr.read();
                }

                words.put(s,(int) (words.get(s)) );
                x = rdr.read();   
                
        }
    }
}

   
    
