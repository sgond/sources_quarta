import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InterruptedException {

        Scanner s = new Scanner(System.in);

        System.out.println("Quanto grande dev'essere l'array?");
        int length = s.nextInt();
        int[] array = arrayGenerator(length);

        System.out.println("In quante parti dev'essere diviso?");
        int arrayDiv = s.nextInt();

        System.out.println("\n" + Arrays.toString(array));

        ThreadSort ts = new ThreadSort(array, arrayDiv);

        System.out.println(Arrays.toString(ts.sort()));
    }

    public static int[] arrayGenerator(int size){
        Random r = new Random();

        int[] out = new int[size];
        for (int i = 0; i < size; i++) {
            out[i] = r.nextInt(size);
        }

        return out;
    }
}