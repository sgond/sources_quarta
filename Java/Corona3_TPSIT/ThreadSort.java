/**
 * Classe per riordinare l'array usando due thread contemporaneamente
 */

public class ThreadSort{

    int[] array;

    int arrayDiv, nSorts = 0;

    ThreadSort(int[] array, int arrayDiv){
        this.array = array;
        this.arrayDiv = arrayDiv;
    }

    private class Sort implements Runnable{
        int sortNumber;

        Sort(){
            sortNumber = nSorts;
        }
        @Override
        public void run() {
            int startPart = sortNumber * (array.length/arrayDiv);
            int endPart;

            if((array.length%arrayDiv) > 0 && sortNumber == nSorts)
                endPart = array.length;
            else
                endPart = startPart + (array.length/arrayDiv);
                
            for (int i = startPart; i < endPart; i++)
                    for (int j = startPart; j < endPart; j++) 
                            if(array[i] < array[j]){
                                int keep = array[j];
                                array[j] = array[i];
                                array[i] = keep;
                            }
                
            for (int i = startPart; i < array.length; i++)
                    for (int j = startPart; j < array.length; j++) 
                            if(array[i] < array[j]){
                                int keep = array[j];
                                array[j] = array[i];
                                array[i] = keep;
                            }
        }
    }

    int[] sort() throws InterruptedException {
        Thread[] threads;

        if(array.length / arrayDiv == 1 && array.length / arrayDiv == 0)
            threads = new Thread[1];
        else
            threads = new Thread[arrayDiv];

        for (int i = 0; i < threads.length; i++) {
                threads[i] = new Thread(new Sort());
                nSorts++;
        }

        for (Thread thread : threads) 
                thread.start();
        
        for (Thread thread : threads) 
                thread.join();

        return array;
    }
}
