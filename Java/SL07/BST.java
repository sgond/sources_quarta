
/**
 * BST
 */

import java.util.Scanner;

public class BST{

    Scanner sc = new Scanner(System.in);

    private String stringAnimals= "E' un ";
    Node root;

    private class Node{
        Node yes, no;
        String value;
        boolean animal;

        Node(String value){
                this.value = value;
        }

        Node(Node yes, Node no, String value){
                this.yes=yes;
                this.no=no;
                this.value=value;
        }

        /**
         * @param left the left to set
         */
        public void setLeft() {
            this.yes = yes;
        }

        public void setLeftV(String value) {
            yes.setValue(value);

        }

        /**
         * @param right the right to set
         */
        public void setRight() {
            this.no = no;
        }

        public void setRightV(String value) {
            no.setValue(value);
        }

        @Override
        public String toString() {
            return "Node [left="+"(" + "value:" + yes.value+")" + "right="+"(" + ", value:" + no.value+")"+"]";
        }

        public Node getLeft() {
            return yes;
        }

        public Node getRight() {
            return no;
        }
        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public boolean getAnimal(){
            return animal;
        }
    }

    public BST(Node root) {
        this.root=root;
    }

    public BST() {
        root = new Node("E' un animale domestico?");
        root.yes = new Node(stringAnimals + "cane");
    }

    void checkRoot(String value){
        if (root == null) 
            root= new Node(value);
    }

    String takeQuestion(Node n){
            String animal;
            String question;

            System.out.print("Che animale è? ");
            animal = sc.nextLine();

            sc.nextLine();

            System.out.print("Che domanda metteresti per " + animal);
            question = sc.nextLine();

            n.yes = new Node(question);
            return stringAnimals + animal;
    }

    void start(){
        Node n = new Node(root.getValue());
        char answer;

        while(true) {
                while(n!=null){
                        System.out.println(n.getValue());
                        answer =  sc.next().charAt(0);

                        if(answer == 's'){
                            if(n.getAnimal()){    
                                System.out.println("Grande");
                            }
                            n=n.yes;
                        }
                        if(answer == 'n'){
                            n = n.no;
                        }
                }
                
                n = new Node(takeQuestion(n));
                n = root;

        }
        
    }
}