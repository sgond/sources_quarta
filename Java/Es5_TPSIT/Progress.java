

import java.awt.*; 
import javax.swing.*;              
import java.awt.event.*; 

public class Progress{ 

    static Timer t;
    static Domanda[] domande = new Domanda[10];
    static JTextArea ta = new JTextArea ("",10,50);
    static JTextArea ta1 = new JTextArea ("",10,50);
    static JTextArea ta2 = new JTextArea ("",10,50);
    static JTextArea ta3 = new JTextArea ("",10,50);
    public static void main(String arg[]) throws Exception
    {    
        
        domande[1] = new Domanda("domanda", 7, 1, "ris1", "ris2");

        JFrame f = new JFrame("Quiz Strumenti Musicali");    
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setBounds(200,100,400,600);           // posizione e dimensione del frame              
        f.setLayout(null);                      // impaginazione        
                                                // crea una progressbar
        JProgressBar bar = new JProgressBar(JProgressBar.HORIZONTAL,0,10000); 
        bar.setValue(0);                        // valore da cui iniziare   
        bar.setStringPainted(true);             // mostra la percentuale sulla barra
 // crea una area di testo con 10 righe e 50 colonne
        ta.setEditable(false);                  // la TextArea non � modificabile dall'utente

        JButton bottone = new JButton("Invio");

        JLabel l= new JLabel("Scegli una Risposta (Numero):");
        JTextField ris= new JTextField(3);

        
        
        bottone.addActionListener( new ActionListener()     // Ascoltatore evento bottone premuto
                                     {
                                         public void actionPerformed(ActionEvent e)
                                         {
                                             String Comando=e.getActionCommand();
                                             if (Comando.equals("Invia"))
                                             {
                                                 JOptionPane.showMessageDialog(null, "Col mouse hai selezionato il bottone");
                                             }
                                          }
                                    });
             
        
        
        ta.setBounds(20,50,320,200);            //  (posx, posy, larghezza, altezza)
        f.add(ta);                              //  aggiunge i vari componenti al Frame

        ta1.setBounds(30,60,320,200);            //  (posx, posy, larghezza, altezza)
        f.add(ta1);
        
        stampaDomanda(domande[1]);
        bar.setBounds(100,300,130,30);
        f.add(bar);
        
        l.setBounds(100,350,200,30);                                    
        f.add(l);
        
        ris.setBounds(100,400,130,30);
        f.add(ris);
                                    
        bottone.setBounds(100,450,130,30);                            
        f.add(bottone);
 
        f.setVisible(true);

        t=new Timer(2,new ActionListener()          // ascoltatore per il timer 
                    {                               // che viene eseguito ogni 2 milli-secondi      
                       public void actionPerformed(ActionEvent ae) 
                       {
                        try {Thread.sleep(1000);}
                        catch(Exception e){}
                        if (bar.getValue() > bar.getMaximum())  t = null;                           
                        else bar.setValue(bar.getValue() + 1000);  //incrementa la barra  
                       }
                    }
                );
 
        t.start();			

    }

    static void stampaDomanda(Domanda d){
        ta.setText(d.getDomanda());

        ta1.setText(d.getRispostaIndex(0));

    }
    

} 