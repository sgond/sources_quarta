
import javax.swing.ImageIcon;

/**
 *
 * @author daria.afatarli & cristian.gondiu
 */
public class Domanda {
	private String domanda;
	private int tempo;
	private int risCorretta;
	private ImageIcon img;
	
	private String[] risposte;

	public Domanda(String domanda, int tempo, int risCorretta, String[] risposte) {
		this.domanda = domanda;
		this.tempo = tempo;
		this.risCorretta = risCorretta;
		this.risposte = risposte;
	}
	
	public Domanda(String domanda, int tempo, int risCorretta, String ris1, String ris2, String ris3, String ris4) {
		this.domanda = domanda;
		this.tempo = tempo;
		this.risCorretta = risCorretta;
		
		risposte = new String[4];
		
		risposte[1] = ris1;
		risposte[2] = ris2;
		risposte[3] = ris3;
		risposte[4] = ris4;
	}
	
	public Domanda(String domanda, int tempo, int risCorretta, String ris1, String ris2, String ris3) {
		this.domanda = domanda;
		this.tempo = tempo;
		this.risCorretta = risCorretta;
		
		risposte = new String[3];
		
		risposte[1] = ris1;
		risposte[2] = ris2;
		risposte[3] = ris3;
	}
	
	public Domanda(String domanda, int tempo, int risCorretta, String ris1, String ris2) {
		this.domanda = domanda;
		this.tempo = tempo;
		this.risCorretta = risCorretta;
		
		this.risposte = new String[2];
		
		this.risposte[0] = ris1;
		this.risposte[1] = ris2;
	}
	
	public Domanda(String domanda, int tempo, int risCorretta, String[] risposte, ImageIcon img) {
		this.domanda = domanda;
		this.tempo = tempo;
		this.risCorretta = risCorretta;
		
		this.img = img;
		
		this.risposte = risposte;
	}

	public String getDomanda() {
		return domanda;
	}

	public int getTempo() {
		return tempo;
	}

	public int getRisCorretta() {
		return risCorretta;
	}

	public ImageIcon getImg() {
		return img;
	}

	public String[] getRisposte() {
		return risposte;
	}

	public String getRispostaIndex(int index){
		return risposte[index];
	}
	
	
	
	
	
	
}
