/**
 * Grafo
 */
public class Grafo {

    int[][] matrix;
    int nNodes;

    Grafo(int length){
        matrix = new int[length][length];
    }

    void addNode(){
        for (int i = 0; i < matrix.length; i++) {           
                if(matrix[i][i]==0){
                    matrix[i][i] = -1;                                                      //Necessario per confermare l'esistenza di un nodo nel caso non sia collegato a nulla

                    nNodes++;
                    return;
                }
                //TODO:Copia tutto in array più grande se non trova posto
        }
    }

    void addNode(int nodeToConnect){
        for (int i = 0; i < matrix.length; i++) {
                if(matrix[i][i] == 0)
                    matrix[i][i] = -1;                                                      //Necessario per confermare l'esistenza di un nodo nel caso non sia collegato a nulla
                
                    try {
                        connect(i, nodeToConnect); 
                        nNodes++;
                        return;

                    } catch (Exception e) {
                        return;
                    }
        }
                //TODO:Copia tutto in array più grande se non trova posto
    }

    void connect(int n1, int n2){
        if(matrix[n1][n1] == -1 && matrix[n2][n2] == -1){
            matrix[n1][n2] = 1;
            matrix[n2][n1] = 1;

            if(n2 != 0)      
                        for (int j = n2 - 1; j >= 0; j--) {
                                if(matrix[j][j+1] == 1){
                                    matrix[j][n1] = n1-j;
                                    matrix[n1][j] = n1-j;
                                }
                        }
        }
        else
            System.out.println("Nodo/i inesistente");
            //TODO: Lanciare eccezione
    }

    boolean isConnected(){
        for (int i = 0; i < matrix.length; i++) {
                for (int j = 0; j < matrix.length; j++) {
                        if(j != i){
                            if(matrix[j][i] != 1){
                                return false;
                        }
                    }
                }
        }

        return true;
    }

    public void print(){
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }
}