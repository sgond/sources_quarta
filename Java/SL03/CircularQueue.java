/**
 * Queue
 */
public class CircularQueue<T> {

    private Node head;
    private Node tail;
    private int size;

    CircularQueue(){}

    public class Node{
        Node next;
        T s;

        Node(T s){
                this.s=s;
        }

        Node(Node next, T s){
                this.next=next;
                this.s=s;
        }

        @Override
        public String toString() {
            return "Node [next=" + next.s +", s=" + s + "]";
        }

        
    }

    void enqueue(T s){
        if(size<2){
            if(head==null)
                head=new Node(s);
            
            else if(tail==null)
                tail=new Node(head, s);  
                head.next=tail;     
        } else{
            Node n = new Node(head, s);
            tail.next=n;
            tail=n;
        }

        size++;
    }

    void rotate(){
        Node n=head;
        head=head.next;
        tail=n; 
    }

    @Override
    public String toString() {
        Node n=head;
        String out="";
        for (int i = 0; i < size; i++) {
            out=out+" "+n.s;
            n=n.next;
        }
        return out;
    }

    public Node getHead() {
        return head;
    }

    public int getSize() {
        return size;
    }

    public Node getTail() {
        return tail;
    }
}