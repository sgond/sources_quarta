/**
 * FileModifierException
 */
public class FileModifierException extends Exception{
    
    FileModifierException(String message) {
        super(message);
    }
}