/**
 * Main
 */
public class Main {

    public static void main(String[] args) {
        Person Alice = new Person(6, 5);
        Person Bob=new Person(15, 23);

        Alice.A = Alice.CalcoloAB(Alice.getPriv(), Alice.getPubl(), Bob.getPubl());
        Bob.A = Bob.CalcoloAB(Bob.getPriv(), Alice.getPubl(), Bob.getPubl());

        System.out.println(Alice.A);
        System.out.println(Bob.A);

        Alice.K=Alice.CalcoloK(Alice.A, Bob.getPubl(), Alice.getPriv());
        Bob.K=Bob.CalcoloK(Bob.A, Bob.getPubl(), Bob.getPriv());

        System.out.println(Alice.K);
        System.out.println(Bob.K);
        
    }
}