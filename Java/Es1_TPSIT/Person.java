/**
 * Bob
 */
public class Person {

    double K;
    double A;

    private double priv;
    double publ;

    public Person(int priv, int publ) {
        this.priv = priv;
        this.publ = publ;
    }

    public double CalcoloAB(double priv, double pubA, double p){
        return Math.pow(pubA, priv)%p;
    }

    public double CalcoloK(double n, double p, double priv){
        return Math.pow(n,priv)%p;
    }

    double getPriv(){
        return priv;
    }

    double getPubl(){
        return publ;
    }

}