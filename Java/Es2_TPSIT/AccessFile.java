/**
 * AccessFile
 */

import java.io.IOException;
import java.io.RandomAccessFile;


public class AccessFile {

    int nameLength, addressLength;
    int pos;
    int nPeople=0;

    public static void main(String[] args) {

        Person[] people= new Person[3];
        personFactory(people);

        String fileName= "./Studenti.txt";
        String name="";
        RandomAccessFile file= new RandomAccessFile(fileName, "rw");
        file.seek(0);

    void WriteOnFile(Person p, RandomAccessFile file){
        file.seek(nPeople*100);
        file.write(p.getBytes()+"\r\n");
        nPeople++;
    }

    void modifyPerson(byte personToSelect, int fieldToModify){
        file.seek((pToSelect-1)*100);

        switch (fieldtoModify) {
            case 0:                                                             //NAME
                //chiedere di inserire nome

                Person p= new Person(String name,)
                break;
        
            default:
                break;
        }

    }

}

class Person{

    String name;
    String surname;
    String address;
    int age;
    char gender;

    void personFactory(Person[] people){
        for (int i = 0; i < people.length; i++) 
                people[i]=new Person("name"+i, "surname"+i, "address"+i, i, 'M');
    }

    public Person(String name, String surname, String address, int age, char gender) {
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.age = age;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return address + " " + age + " " + gender + " " + name + " " + surname;
    }
}