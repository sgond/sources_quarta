
import java.io.IOException;
import java.io.RandomAccessFile;

public class ProvaRandomFile {

	public static void main(String[] args) {
		try {
		
            String FileName = "./testo.txt";
            byte[] bytes = new byte[10];
            
            RandomAccessFile file = new RandomAccessFile(FileName, "r");
		    file.seek(0);
            file.read(bytes);                           // Legge i primi 10 bytes 
            file.close();
            String s= new String(bytes);
            System.out.println(s);


            String dato = " INTRUSO";
            file = new RandomAccessFile(FileName, "rw");
            file.seek(30);
            file.write(dato.getBytes());                //Scrive 8 caratteri lascia \r\n  
            file.close();

			dato = "   by me\r\n";                      // appende una linea di 8 caratteri
            file = new RandomAccessFile(FileName, "rw");
            file.seek(file.length());
            System.out.println("current pointer = "+file.getFilePointer());
            file.write(dato.getBytes());
            file.close();

		} // try 
        catch (IOException e) {e.printStackTrace();}
	} //main
}// class