package divisori;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    static int nThreads = 0;

    static int limite;
    static int[][] divisori;

    static ArrayList[] divisoriList;
    
    public static void main(String[] args) {
        Scanner scn = new Scanner(System.in);

        System.out.print("Inserire limite: ");
        limite = scn.nextInt();
        divisori = new int[limite][limite];

        divisoriList = new ArrayList[limite];

        long timeStart = System.currentTimeMillis();

        for (int i = 1; i <= limite; i++) {
            Thread t = new Thread(new ThreadDivisoriList(i));
            nThreads++;
            t.start();
        }
        while(nThreads != 0){}

        printMatrixList();

        System.out.println(System.currentTimeMillis() - timeStart);
    }

    private static void printMatrix(){
        for (int i = 0; i < divisori.length; i++) {
                for (int j = 0; j < divisori[i].length; j++) {
                        if(divisori[i][j] != 0)
                            System.out.print(divisori[i][j] + " ");
                }
                System.out.println();
        }

        System.out.println();
    }

    private static void printMatrixList(){
        for (int i = 0; i < divisoriList.length; i++)
                System.out.println(divisoriList[i].toString());

        System.out.println();
    }

    private static class ThreadDivisori extends Thread {
        private int nToDivide;

        ThreadDivisori(int nToDivide){
            this.nToDivide = nToDivide;
        }

        @Override
        public void run() {
            int divider = 1, position = 0;

            while(divider <= nToDivide){
                    if(nToDivide % divider == 0){
                        divisori[nToDivide-1][position] = divider;
                        position++;
                    }
                    divider++;
            }

            nThreads--;
        }
    }

    private static class ThreadDivisoriList extends Thread{
        private int nToDivide;

        ThreadDivisoriList(int nToDivide){
            this.nToDivide = nToDivide;
            divisoriList[nToDivide-1] = new ArrayList();
        }

        @Override
        public void run() {
            int divider = 1;

            while(divider <= nToDivide){
                    if(nToDivide % divider == 0)
                        divisoriList[nToDivide-1].add(divider);

                    divider++;
            }

            nThreads--;
        }

    }
}
