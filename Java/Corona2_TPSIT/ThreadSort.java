/**
 * Classe per riordinare l'array usando due thread contemporaneamente
 */

public class ThreadSort{

    int[] array;

    int[] array1;
    int[] array2;

    ThreadSort(int[] array){
        this.array = array;
    }

    private class Sort implements Runnable{
        boolean active = true;

        int[] arrayToSort;

        Sort(int[] arrayToSort){
            this.arrayToSort = arrayToSort;
        }

        @Override
        public void run() {
            active = true;

            int keep;

            for (int i = 0; i < arrayToSort.length; i++) {
                    for (int j = i; j < arrayToSort.length; j++) {
                            if(arrayToSort[i] > arrayToSort[j]){
                                keep = arrayToSort[j];
                                arrayToSort[j] = arrayToSort[i];
                                arrayToSort[i] = keep;
                            }
                    }
            }

            active = false;
        }
    }

    private void splitArray(int[] array){
        array1 = new int[(array.length/2)];
        if(array.length%2 == 1)
            array2 = new int[(array.length/2)+1];
        else
            array2 = new int[(array.length/2)];

        for (int i = 0; i < array.length; i++) {
                if(i < array.length/2)
                    array1[i] = array[i];

                else
                    array2[i-array1.length] = array[i];
        }
    }

    private void sortSmallers(int[] array1, int[] array2){
        for (int i = 0; i < array1.length; i++) {
                int smallestPos = 0;
                boolean foundSmallest = false;
                
                for (int j = 0; j < array2.length; j++) {

                        if(array2[j] < array1[i] && array2[j] <= array2[smallestPos]){
                            smallestPos = j;
                            foundSmallest = true;
                        }
                }

                if(foundSmallest){
                    int keep = array1[i];

                    array1[i] = array2[smallestPos];
                    array2[smallestPos] = keep;
                }
        }
    }

    private int[] mergeArrays(int[] array1, int[] array2){
        int[] out = new int[array.length];

        for (int i = 0; i < out.length; i++) {
                if(i < array1.length)
                    out[i] = array1[i];

                else
                    out[i] = array2[i-array1.length];
        }

        return out;
    }

    int[] sort() throws InterruptedException {
        int[] out = new int[array.length];

        splitArray(array);
        sortSmallers(array1, array2);

        Sort s1 = new Sort(array1);
        Sort s2 = new Sort(array2);

        Thread t1 = new Thread(s1);
        Thread t2 = new Thread(s2);

        t1.start();
        t2.start();

        t1.join();
        t2.join();

        out = mergeArrays(s1.arrayToSort, s2.arrayToSort);

        return out;
    }
}
