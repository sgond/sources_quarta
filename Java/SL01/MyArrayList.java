/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myarraylist;

/**
 *
 * @author cristian.gondiu
 */
public class MyArrayList {
	private String[] ArrayList = new String[10];
    private int size=0;

    MyArrayList(){}

    MyArrayList(int size){
            ArrayList = new String[size];
    }
    
    void add(String toAdd){
            if(size<ArrayList.length){
                ArrayList[size]= toAdd;
                size++;
            }
            else{
                resize();
                add(toAdd);
            }
    }

    void add(String toAdd, int index){
            if(index>ArrayList.length){
                resize();
                add(toAdd, index);
            }
            else
                ArrayList[index-1]=toAdd;

            if(index<1)
                throw new MyArrayListException("Posizione fuori dall'array");       
    }

    String remove(int index){                                                          //Ritorna la string rimossa e se l'operazione è riuscita      
            String removed=ArrayList[index];
            if(index>size)
                throw new MyArrayListException("Posizione fuori dall'array");
            
            else{
                for (int i = index-1; i < size; i++) {
                        ArrayList[i]=ArrayList[i++];
                }
                size--;
                return "La stringa: "+removed+" in posizione: "+index+" è stata rimossa ";
            }
    }

    void clear(){
            MyArrayList cleared=new MyArrayList(10);
    }

    private void resize(){
            String[] newArrayList= new String[ArrayList.length+10];
		System.arraycopy(ArrayList, 0, newArrayList, 0, size);
            ArrayList=newArrayList;
    }

    boolean contains(String toSearch){
		for (String ArrayList1 : ArrayList) {
			if (ArrayList1.equals(toSearch)) {
				return true;
			}
		}
            return false;
    }

    int indexOf(String toSearch){
            for (int i = 0; i < size; i++) {
                if(toSearch.equals(get(i+1)))
                    return (i+1);
            }
            
            throw new MyArrayListException("Stringa inesistente");

    }

    String get(int index) {
            if(!(index-1>size) || !(index-1<0))
                return ArrayList[index-1];

            throw new MyArrayListException("Posizione fuori dall'array");
    }

    void addAll(MyArrayList toAdd){
        if (toAdd.size > ArrayList.length)
            for (int i = 0; i < toAdd.ArrayList.length; i++) {
                    add(toAdd.get(i));
            }
        else
            resize();
            addAll(toAdd);
    }

    int size(){
            return this.size;
    }

    @Override
    public String toString() {
        String toPrint = "[";
        for (int i = 0; i < size; i++) {
            toPrint = toPrint + get(i + 1);
        }
        toPrint = toPrint + "]";

        return toPrint;
    }

    String first(){
            return get(1);
    }

    String last(){
            return get(size+1);
    }
}
