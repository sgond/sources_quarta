package filesystem;

import java.util.ArrayList;

/**
 * FileSystem
 */
public class FileSystem {

    Directory root = new Directory("Root", null);
    Directory current = root;

    public String pwd(){
        return pwd(current);
    }

    private String pwd(Directory current){
        ArrayList<String> directories = new ArrayList<>();
        StringBuilder out = new StringBuilder();

        while(current.getParent()!=null){
                directories.add(current.getName());
                current = current.getParent();
        }

        for (int i = directories.size()-1; i > 0; i--)
                out.append(directories.get(i));
        
        return out.toString();
    }

    private void cd(Directory cd){
        current = cd;
    }

    public void cd(String name){
        if(name.equals("..")){
            if(current.getParent()!=null)
                System.out.println("Nessuna cartella parente");
            else
                current=current.getParent();
        }
        else{
            if(checkDirectory(name)){
                current = getDirFromName(name);
            }
        }

    }

    public boolean checkGenericFile(String toCheck){
        ArrayList arr = current.getArrayFiles();
        for(int i = 0; i>arr.size()-1; i--){
            IFile a = (IFile) arr.get(i);
                if (a.getName().equals(toCheck))
                    return true;
        }
        return false;
    }

    public boolean checkDirectory(String toCheck){
        ArrayList arr = current.getArrayFiles();

        for(int i = 0; i>arr.size()-1; i--){
                Directory a = (Directory) arr.get(i);
                if(a.getClass() == Directory.class)
                    if (a.getName().equals(toCheck))
                        return true;
        }
        return false;
    }

    public Directory getDirFromName(String dirToGet){
        ArrayList files = current.getArrayFiles();
        for (Object file : files)
            if (file.getClass() == current.getClass())
                if ((((Directory) file).getName()).equals(dirToGet))
                    return (Directory) file;

        return null;
    }

    public void mkdir(String directoryName){
        current.addDirectory(directoryName);
    }

    public void touch(String fileName){
        current.addFile(fileName);
    }

    public String ls(){
        return current.toStringFiles();
    }

    public void mv(String oldName, String newName){

    }

    public void mv(File toChangeName, String name){

    }

    public void rm(String fileName){

    }

}