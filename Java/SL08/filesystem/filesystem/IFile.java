/**
 * IFile
 */
package filesystem;

public interface IFile {

    public String getName();
} 