package filesystem;

import java.util.ArrayList;
import java.util.Date;

/**
 * Directory
 */
public class Directory implements IFile {

    private String name;
    private Directory parent;
    private Date created, lastModify;

    private ArrayList<IFile> files;

    public Directory(String name, Directory parent) {
        this.name = name;
        this.parent = parent;
    }

    public Directory(String name){
        this.name = name;
    }

    @Override
    public String toString() {
        return "Directory [name=" + name + "]";
    }

    public String toStringFiles(){
        return files.toString();
    }

    public String getInFileString(int index){
        return (files.get(index)).toString();
    }

    public IFile getIndexFile(int index){
        return files.get(index);
    }


    public ArrayList getArrayFiles(){
        return files;
    }

    public void addDirectory(String name){
        files.add(new Directory(name));
    }

    public void addFile(String name){
        files.add(new File(name));
    }

    public String getName() {
        return name;
    }

    public Directory getParent() {
        return parent;
    }

    public Date getCreated() {
        return created;
    }

    public Date getLastModify() {
        return lastModify;
    }   
}