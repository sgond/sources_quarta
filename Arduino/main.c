#include <stdio.h>
#include <string.h>
#include "crc.c"

int main(void)
{
 // CRC16 di: "il libro rosso" = 5B74

    unsigned char lettera[13];
    unsigned int len;
    char checksum[4];
    unsigned long res=crcSlow(frase, len);   // calcola il checksum res di frase lunga len

    
    ltoa(res,buffer,16);                     // converte res in esadecimale

    buffer[0]=toupper(buffer[0]);            // converte le cifre esadecimali in maiuscolo
    buffer[1]=toupper(buffer[1]);
    buffer[2]=toupper(buffer[2]);
    buffer[3]=toupper(buffer[3]);

    printf("Checksum decimale=%d\nChecksum esadecimale=%s\n", res, buffer);

    return 0;
}
