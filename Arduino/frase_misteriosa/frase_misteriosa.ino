#include <VirtualWire.h>
#include "crc.c"


#define RX_PIN 10

#define posSegment 4
#define lenSegment 8
#define infoSegment 9

const int charLen = 13;

int messaggeLen;

uint8_t charRX[charLen];
uint8_t message[50];

bool collectLen=false;

void setup() {
        Serial.begin(9600);

        vw_set_rx_pin(RX_PIN);
        vw_setup(2000);
        vw_rx_start();
}

void loop() {
        rx();
        
        if (checksum()){
                for (int i = 0; i < posSegment; i++) {
                                String s = s + (char)charRX[i];
                                int pos = s.toInt();

                                message[pos] = charRX[infoSegment-1];
                }
                
        }
        collectLength();
        
        print();
}

void rx() {
            if(vw_have_message())
                vw_get_message(charRX, sizeof(charRX));
}

bool checksum() {
        int toHash=9;

        unsigned char message[toHash];
        unsigned long exaCheck;

        for(int i=0; i<toHash; i++)
                        message[i]=charRX[i];
        
        
        exaCheck = crcSlow(message, toHash);
        char checksumCRC[(sizeof(charRX))-toHash];
        
        ltoa(exaCheck, checksumCRC, 16);

        checksumCRC[0]=toupper(checksumCRC[0]);
        checksumCRC[1]=toupper(checksumCRC[1]);
        checksumCRC[2]=toupper(checksumCRC[2]);
        checksumCRC[3]=toupper(checksumCRC[3]);

        for(int i=toHash; i<charLen;i++)
                        if(charRX[i]!=checksumCRC[i-toHash])       return false;
        
        return true;
}

void print() {
        for (int i = 0; i < messaggeLen; i++){
                        if(message[i]='\0')    Serial.print("*");

                        else                   Serial.print((char)message[i]);
        }

        Serial.println();
}

void collectLength(){
    if(!collectLen){
                for (int i = posSegment; i < lenSegment; i++) {
                                String s = s + (char)charRX[i];
                                messaggeLen = s.toInt();
                }
        }
}
