#include <VirtualWire.h>
#include <LiquidCrystal.h>
#include <DHT.h>

#define TX_PIN 12
#define RX_PIN 11
#define DHT_PIN 10

#define DHTTYPE DHT11

const byte startS_lgt = 8;
const byte endS_lgt = 6;
const byte temp_length=4;
const byte hu_length = 2;

const byte huTot=startS_lgt+endS_lgt+hu_length;
const byte tempTot=startS_lgt+endS_lgt+temp_length;

char x_Type;

int temp;
int um;

String temperature;
String humidity;

const String start_string = "#inizio#";
const String end_string = "#fine#";

DHT sensor(DHT_PIN, DHTTYPE);

void setup() {
    Serial.begin(9600);
    vw_set_tx_pin(TX_PIN);
    vw_set_rx_pin(RX_PIN);
    vw_setup(2000);

    sensor.begin();
}

void loop() { 
    delay(1500);
    char x_Type=x_type();

    if(x_Type==('T'||'t')){
        tx_mode();
    }

    else if(x_Type==('R' || 'r')){
        rx_mode();
    }
}

char x_type(){
    Serial.println("Selezionare ricevitore o trasmettitore('R' / 'T')");
    while(x_Type!=('t'||'T') || x_Type!=('r'||'R'))
        char x_Type = Serial.read();
    Serial.println(x_Type);

    return x_Type;
}

void tx_mode(){

    char charHU[huTot];
    char charTEMP[tempTot];

    while(true){
     
        temp = int(sensor.readTemperature()*100);
        temperature = start_string + "T"+ temp + end_string;
        temperature.toCharArray(charTEMP, tempTot);
        vw_send((uint8_t *)charTEMP, tempTot);

        delay(500);

        um = int(sensor.readHumidity()*100);
        humidity = start_string + "U" + temp + end_string;
        humidity.toCharArray(charHU, huTot);
        vw_send((uint8_t *)charHU, huTot);

        delay(500);
    }
}

void rx_mode(){
    byte messageRX[tempTot];

    vw_rx_start();
    while(true){
            if(vw_have_message()){
                vw_get_message(messageRX, sizeof(messageRX));

                for (int i = 0; i < tempTot; i++){
                        Serial.write(messageRX[i]);
                }
                    Serial.println();
            }
    } 
}


