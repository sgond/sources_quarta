import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main extends Thread {
    
    public static void main(String[] args) throws InterruptedException {
        
        Random rn = new Random();
        Scanner sc = new Scanner(System.in);

        System.out.println("Quanto grande deve essere l'array? ");
        int[] array = new int[sc.nextInt()];

        for(int i = 0; i < array.length; i++){
            array[i] = rn.nextInt(array.length + 1);
        }

        int[] tempoArray1 = new int[array.length / 2];
        int[] tempoArray2 = new int[array.length - tempoArray1.length];

        ArraySort th1 = new ArraySort(tempoArray1);
        ArraySort th2 = new ArraySort(tempoArray2);

        System.arraycopy(array, 0, tempoArray1, 0, tempoArray1.length);
        System.arraycopy(array, tempoArray1.length, tempoArray2, 0, tempoArray2.length);

        th1.start();
        th2.start();
        
        th1.join();
        th2.join();
        
        int i = 0 , j = 0, k = 0;
        
        while (i < th1.length && j < th2.length) {
            if(tempoArray1[i] < tempoArray2[j]) {
                array[k++] = tempoArray1[i];
                i++;
            } else {
                array[k++] = tempoArray2[j];
                j++;
            }
        }
        
        System.arraycopy(th1, i, array, k, (th1.length -i));
        System.arraycopy(th2, j, array, k, (th2.length -j));
        
        System.out.print(Arrays.toString(array));

    }
}