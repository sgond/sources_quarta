import java.util.Arrays;

/**
 *
 * @author danieledicostanzo
 */
public class ArraySort extends Thread {

    public int[] Array1;
    int length;
 
    public ArraySort(int[] tempoArray){
        this.Array1 = tempoArray;
    }

    @Override
    public void run() {
       Arrays.sort(Array1);
    }
    
}